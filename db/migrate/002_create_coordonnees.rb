# coding: utf-8

class CreateCoordonnees < ActiveRecord::Migration
  def self.up
    
    create_table :coordonnees do |t|
      t.integer :contact_id, :null => false
      t.integer :typecoordonnee_id, :null => false
      t.text :texte
      t.timestamps
  	end
    
  end

  def self.down
    drop_table :coordonnees
  end
end

