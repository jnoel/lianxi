# coding: utf-8

class CreateTypecoordonnees < ActiveRecord::Migration
  def self.up
    
    create_table :typecoordonnees do |t|
      t.string :designation, :null => false
      t.timestamps
  	end
  	
  	Typecoordonnee.create :designation => "E-Mail Personnel"
  	Typecoordonnee.create :designation => "E-Mail Professionel"
  	Typecoordonnee.create :designation => "Tél Bureau"
  	Typecoordonnee.create :designation => "Tél Domicile"
  	Typecoordonnee.create :designation => "Tél Portable"
  	Typecoordonnee.create :designation => "Fax"
  	Typecoordonnee.create :designation => "Site Web"
  	Typecoordonnee.create :designation => "Pseudo Skype"
  	Typecoordonnee.create :designation => "Adresse Jabber"
  	Typecoordonnee.create :designation => "Adresse SIP"
    
  end

  def self.down
    drop_table :typecoordonnees
  end
end

