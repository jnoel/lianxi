# coding: utf-8

class CreateContacts < ActiveRecord::Migration
  def self.up
   
    create_table :contacts do |t|
      t.string :societe, :null => false
      t.string :nom
      t.text :adresse
      t.text :remarque
      t.timestamps
    end
    
  end

  def self.down
    drop_table :contacts
  end
end

