# coding: utf-8

class LianxiDb < ActiveRecord::Base
	self.abstract_class = true
end

class Contact < LianxiDb
	has_many :coordonnees
end

class Coordonnee < LianxiDb
	belongs_to :contact
	belongs_to :typecoordonnee
end

class Typecoordonnee < LianxiDb
	has_many :coordonnees
end

class Schema_migration < LianxiDb
	
end
