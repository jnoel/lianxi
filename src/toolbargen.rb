# coding: utf-8

class ToolBarGen_box < Gtk::Toolbar

	def initialize window
	
		super()
		
	  @window = window
	  
	  set_toolbar_style Gtk::Toolbar::Style::BOTH
	
		contacts_tb = Gtk::ToolButton.new( Gtk::Image.new( "./resources/tiers.png" ), "Contacts" )
    configtb = Gtk::ToolButton.new( Gtk::Image.new( "./resources/config.png" ), "Configuration" )
    abouttb = Gtk::ToolButton.new( Gtk::Image.new( "./resources/help22.png" ), "A Propos" )
    quittb = Gtk::ToolButton.new( Gtk::Image.new( "./resources/exit.png" ), "Quitter" )
		
		configtb.signal_connect( "clicked" ) { 
			Assistant.new @window, first=false
		}
		
		abouttb.signal_connect( "clicked" ) { 
			about = About.new window.version
			about.signal_connect('response') { about.destroy }
		}
      
    quittb.signal_connect( "clicked" ) { 
    	window.quit
    }
      
    contacts_tb.signal_connect( "clicked" ) { 
			window.affiche window.liste_contact
			window.liste_contact.refresh
			window.liste_contact.focus
    } 
      
    tool = [contacts_tb, Gtk::SeparatorToolItem.new, configtb, abouttb, Gtk::SeparatorToolItem.new, quittb]
		
		tool.each_index { |i| 
    	self.insert i, tool[i] 
    }
		
	end

end
