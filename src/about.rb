# coding: utf-8
		
class About < Gtk::AboutDialog

	def initialize version
		
		super()

		self.version = version
		self.comments = "Logiciel de gestion des contacts en Ruby/Gtk2"
		lic = ""
		file = File.open("../LICENCE", "r")
		while (line=file.gets)
			lic += line
		end
		file.close
		self.license = lic
		self.website = "http://www.mithril.re"
		self.authors = ["Jean-Noël Rouchon"]
		self.logo = Gdk::Pixbuf.new( "./resources/icon.png" )
		self.set_icon( "./resources/icon.png" )
		
		self.show
	
	end

end		
