# coding: utf-8

class EditionBls < Prawn::Document
	
	attr_reader :fichier
	
	def initialize bls, chemin_temp, casier_seulement=false
	
		super(	:page_layout => :portrait,
				:right_margin => 15,
				:page_size => 'A4'
			 )
		
		i=0
		bls.each { |bl|
			preparation bl
			i +=1
			start_new_page unless i.eql?(bls.count)
		}
		
		@fichier = "#{chemin_temp}/bls.pdf"
		rendu @fichier
	end
	
	def preparation bl
	
		en_tete bl
		corps bl
		pieds bl
		casiers bl
		
	end
	
	def en_tete bl
		
		pas = 10
		text_size = 10
		top = 740
		
		image "resources/images/logo_societe.png", :width => 80, :at => [75,top+40]
		top -= pas+5
		draw_text "SOCIETE D'IMPORTATION DU SUD", :at => [0,top], :size => 14
		top -= pas+5
		draw_text "Capital de 164 645 €", :at => [60,top], :size => text_size
		top -= pas
		draw_text "Z.I. N° 1 - B.P. 349 - 97452 Saint-Pierre Cédex", :at => [10,top], :size => text_size
		top -= pas
		draw_text "Tél. : 02 62 25 12 61 Fax : 02 62 25 66 12", :at => [20,top], :size => text_size
		top -= pas
		draw_text "sisre@sis.re", :at => [80,top], :size => text_size
		top -= pas
		draw_text "Banque : BRED 10107 00492 0053000445 28", :at => [20,top], :size => 8
		top -= pas
		draw_text "SIRET : 310 879 804 00022 - RC : 74 B 72 - Code APE: 4639 B", :at => [0,top], :size => 8
	
		draw_text "Saint-Pierre, le #{DateTime.now.strftime("%d/%m/%Y à %Hh%M")}", :at => [390,760], :size => 8
		top = 710
		draw_text "N° Tournée : #{bl[:num_tournee]}", :at => [410,top], :size => 9
		draw_text "Transporteur : #{bl[:transporteur]}", :at => [410,top-15], :size => 9
		draw_text "Jour de livraison : #{bl[:jour_livraison]}", :at => [410,top-30], :size => 9
		
		top = 640
		rectangle [0,top], 250, 70
		rectangle [290,top], 250, 70
		stroke
		
		top = 625
		left = 10
		size = 10
		pas = 15
		draw_text "BON DE LIVRAISON N° #{bl[:num]}", :at => [left,top], :size => size
		top -= pas
		draw_text "Date de livraison: #{bl[:date].strftime("%d/%m/%Y")}", :at => [left,top], :size => size
		top -= pas
		draw_text "N° Client : #{bl[:code_client]}", :at => [left,top], :size => size
		top -= pas
		draw_text "#{bl[:type_client]}", :at => [left,top], :size => size
		
		top = 633
		left = 300
		formatted_text_box([{ :text => bl[:nom_client]+"\n",
							  :size => 	11,
							  :styles => [:bold]
							},
				            { :text => bl[:adresse_client].gsub("\r\r","\n").gsub("\r","\n"),
				              :size => 10,
				            }],
				            {:at => [left,top]}
				            )
	end
	
	def corps bl
	
		res_array = []
		
		bl[:articles].each do |article|
			l = []
			l << "#{article[:code_article]}           #{article[:gencode]}\n#{article[:dscription]}\nLot n° #{article[:lot]}"
			l << "#{"%.0f" % article[:nb_unites]}"
			l << "#{"%.3f €" % article[:pu_ht]}"
			l << "#{"%.3f €" % article[:pu_rem]}"
			l << "#{"%.2f €" % article[:total_ht]}"
			l << "#{"%.2f" % article[:tva]}"
			res_array << l
		end
	
		data = [[]]
		data[0] << "Code                Gencode\nDésignation"
		data[0] << "Nb total d'unité"
		data[0] << "PU HT"
		data[0] << "PU rem. HT"
		data[0] << "Montant Total HT"
		data[0] << "TVA"
  		data += res_array
  		 		
  		bounding_box([0,bounds.top-205], :width => 550) do
	  		t = table(data) do |table|
	  			table.header = true
	  			table.width = 540
	  			table.row(0).style :borders => [:top, :bottom, :left, :right]
	  			table.row(0).style :align => :center
	  			table.row(1..data.count).style :borders => [:left, :right]
	  			table.row(data.count-1).style :borders => [:left, :right, :bottom]
	  			table.cell_style = { :size => 8, :border_color => "000000"}
	  			table.row(0).style :align => :center
	  			table.column(0).align = :left
	  			table.columns(1..5).align = :center
	  			#table.column(0).row(1..data.count).font = "./resources/fonts/c39hrp24dhtt.ttf"
	  			#table.column(0).row(1..data.count).size = 30
	  			table.columns(1..4).width = 70
	  			table.columns(5).width = 50
			end
		end
		
		top = cursor
	
	end
	
	def pieds bl
	
		note = ""
		
		top = 200
		start_new_page if cursor<top
		left = 10
		size = 10
		pas = 15
		draw_text "Remarque :", :at => [left,top], :size => size
		top -= pas
		draw_text "#{bl[:note]}", :at => [left,top], :size => size
		
		top = 130
		draw_text "Livreur :", :at => [left,top], :size => size
		top -= pas
		draw_text "Signature :", :at => [left,top], :size => size
		
		left = 450
		
		top = 100
		rectangle [left,top], 100, 30
		draw_text "TOTAL €", :at => [left+32,top-18], :size => size, :styles => [:bold]
		top -= 30
		rectangle [left,top], 100, 20
		text_box "#{"%.2f" % bl[:ht]} €", :at => [left+10,top-7], :size => size, :align => :right
		rectangle [left-80,top], 80, 20
		draw_text "Total HT", :at => [left-60,top-13], :size => size
		top -= 20
		rectangle [left,top], 100, 20
		text_box "#{"%.2f" % bl[:tva]} €", :at => [left+10,top-7], :size => size, :align => :right
		rectangle [left-80,top], 80, 20
		draw_text "Total TVA", :at => [left-62,top-13], :size => size
		top -= 20
		rectangle [left,top], 100, 20
		text_box "#{"%.2f" % bl[:ttc]} €", :at => [left+10,top-7], :size => size, :align => :right
		rectangle [left-80,top], 80, 20
		draw_text "Net à Payer", :at => [left-65,top-13], :size => size
		stroke
		
		top = 100
		left = 10
		rectangle [left,top], 220, 20
		draw_text "Détails de TVA", :at => [left+80,top-13], :size => size, :styles => [:bold]
		top -= 20
		rectangle [left,top], 70, 20
		draw_text "HT", :at => [left+28,top-13], :size => size
		rectangle [left+70,top], 70, 20
		draw_text "TVA", :at => [left+96,top-13], :size => size
		rectangle [left+140,top], 80, 20
		draw_text "MONTANT TVA", :at => [left+146,top-13], :size => size
		top -= 20
		rectangle [left,top], 70, 45
		text_box "0.00 €", :at => [left+10,top-7], :size => size, :align => :right, :width => 55
		text_box "#{"%.2f" % bl[:ht]} €", :at => [left+10,top-20], :size => size, :align => :right, :width => 55
		text_box "0.00 €", :at => [left+10,top-32], :size => size, :align => :right, :width => 55
		rectangle [left+70,top], 70, 45
		draw_text "0.00 %", :at => [left+90,top-13], :size => size
		draw_text "2.10 %", :at => [left+90,top-26], :size => size
		draw_text "8.50 %", :at => [left+90,top-40], :size => size
		rectangle [left+140,top], 80, 45
		text_box "0.00 €", :at => [left+160,top-7], :size => size, :align => :right, :width => 50
		text_box "#{"%.2f" % bl[:tva]} €", :at => [left+160,top-20], :size => size, :align => :right, :width => 50
		text_box "0.00 €", :at => [left+160,top-32], :size => size, :align => :right, :width => 50
		stroke
		
		draw_text "Pénalités de retard au taux de 1.50% / mois (loi du 15/05/01)", :at => [left,top-58], :size => size
		
	end
	
	def casiers bl
		if bl[:casiers].count>0
		
			res_array = []
		
			(0..3).each do |i|
				l = []
				if i< bl[:casiers].count
					l << "#{bl[:casiers][i][:code]}"
					l << "#{bl[:casiers][i][:date]}"
					res_array << l
				end
			end
	
			data = [[]]
			data[0] << "N° casier"
			data[0] << "Date de sortie"
	  		data += res_array
		
			text_box "Vous avez #{bl[:casiers].count} casier(s) en votre possession, dont les casiers suivants:", :at => [370,180], :size => 7, :align => :right, :width => 75
		
			bounding_box([450, 200], :width => 100) do
		  		t = table(data) do |table|
		  			table.header = true
		  			table.width = 100
		  			table.row(0).style :borders => [:top, :bottom, :left, :right]
		  			table.row(0).style :align => :center
		  			table.row(1..data.count).style :borders => [:left, :right]
		  			table.row(data.count-1).style :borders => [:left, :right, :bottom]
		  			table.cell_style = { :size => 7, :border_color => "000000"}
				end
			end
		end
	end
	
	def rendu fichier

		render_file fichier
	
	end
	
end
