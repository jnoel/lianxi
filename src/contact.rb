# coding: utf-8

class Contact_box < Gtk::VBox

	attr_reader :casier

	def initialize window
	
		super(false, 3)
		
		set_border_width 10
		
		@id = 0
		
		@window = window

		@valider = Gtk::Button.new
		hboxvalider = Gtk::HBox.new false, 2
		hboxvalider.add Gtk::Image.new( "./resources/ok.png" )
		hboxvalider.add Gtk::Label.new "Valider"
		@valider.add hboxvalider
		@annuler = Gtk::Button.new
		hboxannuler = Gtk::HBox.new false, 2
		hboxannuler.add Gtk::Image.new( "./resources/cancel.png" )
		hboxannuler.add Gtk::Label.new "Annuler"
		@annuler.add hboxannuler
		
		agencement
		
		@valider.signal_connect( "clicked" ) { validate }
		@annuler.signal_connect( "clicked" ) { quit }
	
	end
	
	def agencement
	
		vbox = Gtk::VBox.new false, 3
		@frame = Gtk::Frame.new
		@frame.label = "Nouveau Contact"
		vbox.pack_start( @frame, true, true, 3 )
		
		vbox_corps = Gtk::VBox.new false, 3
		@frame.add vbox_corps
		vbox_corps.pack_start( en_tete, true, true, 3 )		
		
		hbox3 = Gtk::HBox.new false, 2
		align1 = Gtk::Alignment.new 1, 1, 0, 0
		hbox3.pack_start( @annuler, true, true, 3 )
		hbox3.pack_start( @valider, true, true, 3 )
		align1.add hbox3
		
		vbox.pack_start( align1, false, false, 3 )
		
		self.add vbox
	
	end
	
	def en_tete
	
		# Objets
		@societe = Gtk::Entry.new
		@nom = Gtk::Entry.new
		@adresse = Gtk::TextView.new
		@remarque = Gtk::TextView.new
		scroll_adresse = Gtk::ScrolledWindow.new
		scroll_remarque = Gtk::ScrolledWindow.new
		
		# Propriétés
		@remarque.wrap_mode = Gtk::TextTag::WRAP_WORD
		@adresse.wrap_mode = Gtk::TextTag::WRAP_WORD
		scroll_adresse.set_policy(Gtk::POLICY_NEVER,Gtk::POLICY_AUTOMATIC)
		scroll_remarque.set_policy(Gtk::POLICY_NEVER,Gtk::POLICY_AUTOMATIC)
				
		# Agencement		
		vbox = Gtk::VBox.new false, 3
		hbox = Gtk::HBox.new false, 3
		vbox2 = Gtk::VBox.new false, 3
		hbox2 = Gtk::HBox.new true, 10
		vbox3 = Gtk::VBox.new false, 3
		
		hbox.pack_start(Gtk::Label.new("Societé/Nom :"), false, false, 3)
		hbox.pack_start(@societe, true, true, 3)
		hbox.pack_start(Gtk::Label.new("Deuxième nom :"), false, false, 3)
		hbox.pack_start(@nom, true, true, 3)
		
		vbox.pack_start hbox, false, false, 3
		
		align = Gtk::Alignment.new 0, 0, 0, 0
		align.add Gtk::Label.new( "Adresse :" )	
		vbox2.pack_start align, false, false, 3
		scroll_adresse.add @adresse
		vbox2.pack_start scroll_adresse, true, true, 3
		
		align2 = Gtk::Alignment.new 0, 0, 0, 0
		align2.add Gtk::Label.new( "Remarques :" )	
		vbox2.pack_start align2, false, false, 3
		scroll_remarque.add @remarque
		vbox2.pack_start scroll_remarque, true, true, 3
		
		align3 = Gtk::Alignment.new 0, 0, 0, 0
		align3.add Gtk::Label.new( "Coordonées :" )	
		vbox3.pack_start align3, false, false, 3
		vbox3.pack_start tableau_coordonnees, true, true, 3
		
		hbox2.add vbox2
		hbox2.add vbox3
		
		vbox.pack_start hbox2, true, true, 3
		
		vbox
	
	end
	
	def tableau_coordonnees
		
		# list_store 					           (id      type     coordonnees  mail corbeille)
		@list_store = Gtk::TreeStore.new(Integer, String, String, Gdk::Pixbuf, Gdk::Pixbuf, Integer)
		@view = Gtk::TreeView.new(@list_store)
		
		@view.signal_connect ("button-release-event") { |tree,event|
			# Gestion du clic droit sur une ligne
			if event.button.eql?(3)
				#TODO
			end
			# Si clic gauche
			e = npath = @view.get_path_at_pos(event.x, event.y)
			if (e and event.button.eql?(1))
				colonne = e[1]
				#si clic sur colonne mail/web
				if colonne.eql?(@view.get_column(2))
					ligne = @list_store.get_iter(e[0])
					case ligne[5]
						when 1
							if RUBY_PLATFORM.include?("linux") then 
								system "xdg-email", ligne[2]
							elsif RUBY_PLATFORM.include?("mingw") then 
								# TODO
							else
								# TODO
							end
						when 2
							if !ligne[2].include?("http://")
								ligne[2] = "http://" + ligne[2]
							end
							if RUBY_PLATFORM.include?("linux") then 
								system "xdg-open", ligne[2]
							elsif RUBY_PLATFORM.include?("mingw") then 
								# TODO
							else
								# TODO
							end
					end
				#si clic sur colonne supprimer
				elsif colonne.eql?(@view.get_column(3))
					ligne = @list_store.get_iter(e[0])
					supprime_ligne ligne
				end
			end
		}
		
		renderer_type = Gtk::CellRendererCombo.new
		renderer_type.has_entry = false
		renderer_type.xalign = 0
		renderer_type.yalign = 0
		renderer_type.editable = true		
		renderer_type.model = modele_type
		renderer_type.text_column = 1
		renderer_type.signal_connect('edited') { |combo, data, text|
			unless text.to_s.empty?
				first_time = @view.model.get_iter(data)[1].to_s.empty?
				@view.model.get_iter(data)[1] = text
				@view.model.get_iter(data)[4] = @pix_trash
				nouvelle_ligne if first_time
			end
		} 
		col = Gtk::TreeViewColumn.new("Type", renderer_type, :text => 1)
		col.resizable = true
		@view.append_column(col)
		
		renderer_coordonnees = Gtk::CellRendererText.new
		renderer_coordonnees.xalign = 0
		renderer_coordonnees.yalign = 0
		renderer_coordonnees.mode = Gtk::CellRendererText::MODE_EDITABLE
		renderer_coordonnees.editable = true
		renderer_coordonnees.signal_connect('edited') { |combo, data, text|
			@view.model.get_iter(data)[2] = text unless @view.model.get_iter(data)[1].to_s.empty?
		}
		col = Gtk::TreeViewColumn.new("Coordonnée", renderer_coordonnees, :text => 2)
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		# Colonne pour le mail ou le web
		@pix_mail = Gdk::Pixbuf.new("resources/saisie.png", 20, 20)
		@pix_web = Gdk::Pixbuf.new("resources/internet.png", 20, 20)
		renderer_pix = Gtk::CellRendererPixbuf.new	
		renderer_pix.xalign = 0.5
		renderer_pix.yalign = 0
		col = Gtk::TreeViewColumn.new("", renderer_pix, :pixbuf => 3)
		col.resizable = false
		@view.append_column(col)
		
		# Colonne pour la corbeille
		@pix_trash = Gdk::Pixbuf.new("resources/trash-full.png", 20, 20)
		col = Gtk::TreeViewColumn.new("", renderer_pix, :pixbuf => 4)
		col.resizable = false
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
  	scroll.add @view
  	
  	scroll
		
	end
	
	def modele_type
		
		types = Typecoordonnee.order(:id)
		types_model = Gtk::ListStore.new(Integer, String)
		types.each { |type|
			iter = types_model.append
			iter[0] = type.id
			iter[1] = type.designation
		}	
		types_model	
		
	end
	
	def refresh id=0
	
		@id = id
		@societe.text = ""
		@nom.text = ""
		@remarque.buffer.text = ""
		@adresse.buffer.text = ""
		@list_store.clear
		@suppression = []
		
		if id>0 then
			@frame.label = "Contact n° " + id.to_s
			#@lot.sensitive = false
			@contact = Contact.find(id)
			if @contact then
				@societe.text = @contact.societe
				@nom.text = @contact.nom
				@adresse.buffer.text = @contact.adresse
				@remarque.buffer.text = @contact.remarque
				remplir_coordonnees
			else
				@window.message_erreur @contact.error
			end			
		else
			@frame.label = "Nouveau contact"	
			@contact = Contact.new
		end
		
		nouvelle_ligne
	
	end
	
	def nouvelle_ligne
		
		iter = @list_store.append nil
		
	end
	
	def supprime_ligne ligne
	
		dialog = Gtk::MessageDialog.new(@window, 
		                            Gtk::Dialog::DESTROY_WITH_PARENT,
		                            Gtk::MessageDialog::QUESTION,
		                            Gtk::MessageDialog::BUTTONS_YES_NO,
		                            "Voulez-vous réellement supprimer cette ligne ?")
		response = dialog.run
		case response
		  when Gtk::Dialog::RESPONSE_YES
				@suppression << ligne[0] unless ligne[0].to_i.eql?(0)
				@list_store.remove ligne
		end
		
		dialog.destroy
	
	end
	
	def remplir_coordonnees
	
		coordonnees = Coordonnee.where(:contact_id => @id).order(:id)
		coordonnees.each do |c|
			iter = @list_store.append nil
			iter[0] = c.id
			iter[1] = c.typecoordonnee.designation
			iter[2] = c.texte
			case c.typecoordonnee_id
				when 1,2
					iter[3] = @pix_mail
					iter[5] = 1
				when 7
					iter[3] = @pix_web
					iter[5] = 2
				else
					iter[5] = 0
			end
			iter[4] = @pix_trash
		end
	
	end
	
	def validate quitter=true
		
		# Vérification d'erreurs
		res = false
		error = ""
		if !(@id>0) then
			if @societe.text.empty? then
				error += "Vous devez saisir un nom pour ce contact\n"
			end
		end
		
		# Si pas d'erreurs on continue
		if error.empty? then
	
			# sauvegarde des données du contact
			@contact.societe = @societe.text
			@contact.nom = @nom.text
			@contact.remarque = @remarque.buffer.text
			@contact.adresse = @adresse.buffer.text

			res = @contact.save
			
			@id = @contact.id
			
			# Enregistrement des coordonnées
			@list_store.each { |model, path, iter|
				ligne = @view.model.get_iter(path)
					unless ligne[2].to_s.empty?
					id = @view.model.get_iter(path)[0].to_i
					coordonnee = nil
					if id>0
						coordonnee = Coordonnee.find(id)
					else
						coordonnee = Coordonnee.new
					end
					type = Typecoordonnee.where(:designation => ligne[1]).first
					coordonnee.contact_id = @id
					coordonnee.typecoordonnee_id = type.id
					coordonnee.texte = ligne[2]
					coordonnee.save
				end
			}
			
			# Suppression des coordonnées retirées
			@suppression.each do |s|
				p "suppression de la coordonnée id=#{s}"
				Coordonnee.destroy(s)
			end
		
			quit if (!@dial and quitter)
		
		else
			@window.message_erreur error
		end
		return res
	
	end
	
	def focus
		@annuler.grab_focus
	end
	
	def quit
	
		@window.liste_contact.refresh
		@window.affiche @window.liste_contact
		@window.liste_contact.focus
	
	end
	
end

