#!/usr/bin/env ruby
# coding: utf-8

require 'rubygems'
require 'bundler/setup'
require 'active_record'
require 'logger'
require 'gtk2'
require 'date'
require 'yaml'
require 'prawn'
require 'prawn/layout'

require '../db/models.rb'

Dir.foreach('.') { |f|
	require "./#{f}" if File.extname(f).eql?(".rb")
}

GLib.set_application_name "Lianxi"

Gtk.init
win = MainWindow.new :toplevel
Gtk.main
