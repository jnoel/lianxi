# coding: utf-8

class ConfigDb

	attr_accessor :conf, :chemin_config, :chemin_log, :chemin_temp
	
	def initialize window
		@window = window
		@chemin_temp = Dir.home+"/.lianxi"
		Dir.mkdir(@chemin_temp) unless Dir.exist?(@chemin_temp)
		@chemin_config = @chemin_temp + "/configuration.yaml"
		@chemin_log = '../db/database.log'
		@conf = nil
	end
	
	def verif_fichier
		if File.exist?(@chemin_config)
			p "Ouverture du fichier de configuration #{@chemin_config}."
			@conf = YAML.load_file(@chemin_config)
			return true
		else
			p "Le fichier de configuration #{@chemin_config} n'existe pas, il doit être créé."
			Assistant.new @window, first=true
			return false
		end
	end
	
	def connexion
		if verif_fichier
			p "Connexion à la base de données #{@conf['database']} sur le serveur #{@conf['adapter']} #{@conf['host']}:#{@conf['port']}."
			LianxiDb.logger = Logger.new(File.open(@chemin_log,'w'))
			ActiveRecord::Base.establish_connection(@conf)			
			LianxiDb.establish_connection(@conf)

			lancement = false
			begin
				last_version = 0
				versions = Schema_migration.find(:all)
				versions.each { |v|
					last_version = v.version.to_i>last_version ? v.version.to_i : last_version
				}
				if LianxiDb.connected?
					p "Connexion établie. Version de la base de données = #{last_version}"
					last_migrate = 0
					Dir.foreach("../db/migrate") {|x| 
						num = x.to_s[0..2].to_i
						last_migrate=num if num>last_migrate 
					}
					p "Dernier numéro de migration = #{last_migrate}"
					if last_version<last_migrate.to_i
						p "Lancement de l'assistant"
						Assistant.new @window, first=false, migration=true
					elsif last_version.eql?(last_migrate.to_i)
						lancement = true
					else
						@window.message_erreur "Votre logiciel est trop ancien par rapport à la base de donnée, veuillez le mettre à jour"
						Process.exit
					end
				else
					p "Connexion impossible, veuillez vérifier les paramètres."
					p "Lancement de l'assistant"
					Assistant.new @window, first=true
				end
			rescue
				if versions.nil?
					p "La base de donnée n'existe pas ou n'est pas correctement créée."
				elsif versions.length.eql?(0)
					p "La table Schema_migration existe mais elle est vide."
				end
				p "Lancement de l'assistant"
				Assistant.new @window, first=true		
			end
		
			@window.lancement if lancement
		end
	end
	
	def save_config data
		
		File.open(@chemin_config, 'w') do |out|
   			YAML.dump(data, out)
		end
		@conf = YAML.load_file(@chemin_config)
	end
end
