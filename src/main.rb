# coding: utf-8

class MainWindow < Gtk::Window
	
	attr_reader :contact, :liste_contact, :version
	attr_accessor :config_db
	
	def initialize level
	
		super
		
		signal_connect( "destroy" ) { Gtk.main_quit }
		
		@version = "0.1"
		set_title GLib.application_name + " v" + @version
		
		set_window_position Gtk::Window::POS_CENTER
		set_default_size 800, 600
		set_icon( "./resources/icon.png" )
		maximize	
		
		@config_db = ConfigDb.new self
		@config_db.connexion
	
	end
	
	def lancement
		
		@toolbargen = ToolBarGen_box.new self
		@liste_contact = ListeContact_box.new self
		@contact = Contact_box.new self
		
		@vbox = Gtk::VBox.new false, 2
		@vbox.pack_start @toolbargen, false, false, 2
		@vbox.pack_start @liste_contact, true
		
		@liste_contact.focus
		@liste_contact.refresh
		
		add @vbox
		
		show_all
	
	end
	
	def efface_vbox_actuelle
	
		@vbox.remove @vbox.children[@vbox.children.count-1] if @vbox.children.count.eql?(2)
	
	end
	
	def affiche vbox
	
		efface_vbox_actuelle
		@vbox.pack_start vbox, true
        self.show_all
	
	end	
	
	def quit
		
		dialog = Gtk::MessageDialog.new(self, 
                                Gtk::Dialog::DESTROY_WITH_PARENT,
                                Gtk::MessageDialog::QUESTION,
                                Gtk::MessageDialog::BUTTONS_YES_NO,
                                "Voulez-vous réellement quitter #{GLib.application_name.capitalize} ?")
		response = dialog.run
		case response
		  when Gtk::Dialog::RESPONSE_YES
			Gtk.main_quit
		end 
		dialog.destroy
		
	end
	
	def message_erreur message
		p message
		m = "Impossible d'exécuter la requête demandée...\nVoici le message d'erreur affiché :\n" + message
		dialog = Gtk::MessageDialog.new(self, 
                                Gtk::Dialog::DESTROY_WITH_PARENT,
                                Gtk::MessageDialog::ERROR,
                                Gtk::MessageDialog::BUTTONS_OK,
                                m)
		response = dialog.run
		dialog.destroy
	end
	
	def message_info message
		p message
		dialog = Gtk::MessageDialog.new(self, 
                                Gtk::Dialog::DESTROY_WITH_PARENT,
                                Gtk::MessageDialog::INFO,
                                Gtk::MessageDialog::BUTTONS_OK,
                                message)
		response = dialog.run
		dialog.destroy
	end

end
