# coding: utf-8

class Assistant < Gtk::Assistant

	def initialize window, first=true, migration=false
	
		super()
		
		@window = window
		@conf = window.config_db.conf
		@chemin_config = @window.config_db.chemin_config
		@chemin_log = @window.config_db.chemin_log
		@first = first
		@migration=migration
	
		page = [
			PageInfo.new(nil, -1, "Bienvenue dans #{GLib.application_name.capitalize}", Gtk::Assistant::PAGE_INTRO, true),
			PageInfo.new(nil, -1, "Configuration de la base de données", Gtk::Assistant::PAGE_CONTENT, false),
			PageInfo.new(nil, -1, "", Gtk::Assistant::PAGE_CONTENT, true),
			PageInfo.new(nil, -1, "Migration des données", Gtk::Assistant::PAGE_PROGRESS, false),
			PageInfo.new(nil, -1, "Fin", Gtk::Assistant::PAGE_CONFIRM, true)
		]

		set_window_position Gtk::Window::POS_CENTER
		set_default_size 500, 500
		signal_connect('destroy') { 
			self.destroy 
			Gtk.main_quit if (@first or @migration)
		}
		
		intro = Gtk::Label.new
		
		if @first
			intro.text =   "Vous lancez #{GLib.application_name.capitalize} pour la première fois.\n" +
						   "Cet assistant vous permettra de configurer votre logiciel.\n" +
						   "Pour que #{GLib.application_name.capitalize} fonctionne, vous devez avoir un serveur Postgresql\n" +
						   "installé et fonctionnel et avoir préparé une base de donné pour votre société.\n" +
						   "Quand vous serez prêt cliquez sur suivant..."
		elsif @migration
			intro.text =   "Bienvenue dans l'assistant de configuration de #{GLib.application_name.capitalize}.\n" +
						   "Vous voyez cet assistant car une mise à jour doit être effectuée sur votre logiciel.\n" +
						   "Cliquez sur suivant pour continuer le processus."
		else
			intro.text =   "Bienvenue dans l'assistant de configuration de #{GLib.application_name.capitalize}.\n" +
						   "Cliquez sur suivant pour démarrer le processus."
		end

		page[0].widget = intro
		page[1].widget = Gtk::HBox.new(FALSE, 5)
		page[2].widget = Gtk::Label.new(
				"La création ou la migration de la base de données va commencer.\n" +
				"Cliquez sur suivant quand vous serez prêt."
			  )
		page[3].widget = Gtk::HBox.new(FALSE, 5)
		page[4].widget = Gtk::Label.new(
				"La procédure de configuration est terminée.\n" +
				"Nous vous souhaitons une bonne utilisation."
			  )
		# Create the necessary widgets for the second page.
		page[1].widget.pack_start(db(@conf), true, true, 5)
		
		@avancement = Gtk::TextView.new
		@avancement.editable = false
		@avancement.modify_base Gtk::STATE_NORMAL, Gdk::Color.new(0, 0, 0)
		@avancement.buffer.create_tag("titre", {"foreground" => "white", "background" => "black"})
		scroll = Gtk::ScrolledWindow.new
    	scroll.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
    	scroll.add @avancement
		page[3].widget.pack_start(scroll, true, true, 5)

		# Add five pages to the Gtk::self dialog.
		intervalle = @migration ? [0,3,4] : [0,1,2,3,4]
		intervalle.each do |i|
			page[i].index = self.append_page(page[i].widget) 
			self.set_page_title(page[i].widget, page[i].title)
			self.set_page_type(page[i].widget, page[i].type)

			# Set the introduction and conclusion pages as complete so
			# they can be incremented or closed.
			self.set_page_complete(page[i].widget, page[i].complete)
		end

		# Update whether pages 2 through 4 are complete based upon
		# whether there is text in the GtkEntry, the check button is
		# active, or the progress bar is completely filled.

		self.signal_connect('prepare') { 
			n = @migration ? 1 : 3
			lancement_migration(n) if current_page.eql?(n)
			save_config	if current_page.eql?(4)
		}
		self.signal_connect('cancel')  { self.destroy }
		self.signal_connect('close')   { assistant_close }
		
		show_all

	end
	
	def lancement_migration n
		@avancement.buffer.insert(@avancement.buffer.start_iter, `bundle exec rake migrate`, "titre")
		@avancement.buffer.insert(@avancement.buffer.end_iter, "\nMigration terminée. Vous pouvez cliquer sur suivant", "titre")
		set_page_complete(get_nth_page(n), true)
	end
	
	# This function is where you would apply the changes and
	# destroy the assistant.
	def assistant_close
		self.destroy
		# on relance le programme
		exec("ruby","#{GLib.application_name.downcase}.rbw") if (@first or @migration)
		Gtk.main_quit if (@first or @migration)
	end
	
	# Fill up the progress bar, 10% every second when the button
	# is clicked. Then, set the page as complete when the progress
	# bar is filled.
	def button_clicked (button, assistant, progress)
		percent = 0.0
		button.sensitive = false
		num = assistant.current_page
		page = assistant.get_nth_page(num)
		while(percent <= 100.0)
			progress.text = "%.0f%% Complete" % percent
			progress.fraction = percent / 100.0
			while (Gtk.events_pending?)
				Gtk.main_iteration
			end
			sleep 0.1
			percent += 5.0
		end
		system 'rake migrate'
		assistant.set_page_complete(page, true)
	end
	
	def save_config
		data = { 
						 'adapter' => @adapter.active_text,
						 'host' => @host.text,
						 'database' => @database.text,
						 'port' => @port.text,
						 'username' => @username.text,
						 'password' => @password.text,
						}	
		
		File.open(@chemin_config, 'w') do |out|
   			YAML.dump(data, out)
		end
		@conf = YAML.load_file(@chemin_config)
		p "Sauvegarde de la nouvelle configuration terminée."
	end
	
	def test_config
		test = false
		
		LianxiDb.logger = Logger.new(File.open(@chemin_log,'w'))
		LianxiDb.establish_connection(@conf)
		
		last_version = 0
		begin
			versions = Schema_migration.find(:all)
			versions.each { |v|
				last_version = v.version.to_i>last_version ? v.version.to_i : last_version
			}
		rescue
		end
		
		test = LianxiDb.connected?
		message = ""
		
		if test
			num = self.current_page
			page = self.get_nth_page(num)
			self.set_page_complete(page, true)
			message = "Connexion établie, vous pouvez continuer."
		else
			message = "Connexion échouée, vérifiez les paramètres."
		end
		
		p message
		dialog = Gtk::MessageDialog.new(self, 
                                Gtk::Dialog::DESTROY_WITH_PARENT,
                                Gtk::MessageDialog::QUESTION,
                                Gtk::MessageDialog::BUTTONS_CLOSE,
                                message)
		dialog.run
		dialog.destroy
	end
	
	def db conf
				
		table = Gtk::Table.new 10, 2, false
		i = 0
		
		# Le type de serveur
		label_adapter = Gtk::Alignment.new 0, 0.5, 0, 0
		label_adapter.add Gtk::Label.new("Type de serveur:")
		@adapter = Gtk::ComboBox.new
		@adapter.append_text "postgresql"
		@adapter.active = 0
		table.attach( label_adapter, 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 2, 2 )	
		table.attach( @adapter, 1, 2, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 2, 2 )
		
		i += 1
		# L'adresse ip de l'hôte
		label_host = Gtk::Alignment.new 0, 0.5, 0, 0
		label_host.add Gtk::Label.new("Adresse du serveur:")
		@host = Gtk::Entry.new
		@host.text = conf["host"].to_s unless conf.nil?
		table.attach( label_host, 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 2, 2 )	
		table.attach( @host, 1, 2, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 2, 2 )
		
		i += 1
		# Le nom de la base de données
		label_database = Gtk::Alignment.new 0, 0.5, 0, 0
		label_database.add Gtk::Label.new("Base de données:")
		@database = Gtk::Entry.new
		@database.text = conf["database"].to_s unless conf.nil?
		table.attach( label_database, 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 2, 2 )	
		table.attach( @database, 1, 2, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 2, 2 )
		
		i += 1
		# Le port de la base de données
		label_port = Gtk::Alignment.new 0, 0.5, 0, 0
		label_port.add Gtk::Label.new("Port:")
		@port = Gtk::Entry.new
		@port.text = conf["port"].to_s unless conf.nil?
		table.attach( label_port, 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 2, 2 )	
		table.attach( @port, 1, 2, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 2, 2 )
		
		i += 1
		# Le nom d'utilisateur
		label_username = Gtk::Alignment.new 0, 0.5, 0, 0
		label_username.add Gtk::Label.new("Nom d'utilisateur:")
		@username = Gtk::Entry.new
		@username.text = conf["username"].to_s unless conf.nil?
		table.attach( label_username, 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 2, 2 )	
		table.attach( @username, 1, 2, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 2, 2 )
		
		i += 1
		# Le mot de passe
		label_password = Gtk::Alignment.new 0, 0.5, 0, 0
		label_password.add Gtk::Label.new("Mot de passe:")
		@password = Gtk::Entry.new
		@password.visibility = false
		@password.text = conf["password"].to_s unless conf.nil?
		table.attach( label_password, 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 2, 2 )	
		table.attach( @password, 1, 2, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 2, 2 )
		
		i += 1
		# Le bouton de test de la connexion
		align_test = Gtk::Alignment.new 1, 0.5, 0, 0
		test_button = Gtk::Button.new
		hbox_test = Gtk::HBox.new false, 2
		hbox_test.add Gtk::Image.new( "./resources/config.png" )
		hbox_test.add Gtk::Label.new "Tester"
		test_button.add hbox_test
		test_button.signal_connect("clicked") { 
			save_config 
			test_config	
		}
			
		align_test.add test_button
		table.attach( align_test, 1, 2, i, i+1, Gtk::FILL, Gtk::FILL, 2, 2 ) 
		
		table
	
	end

end

class PageInfo

	attr_accessor :widget, :index, :title, :type, :complete
	
	def initialize(w, i, ti, ty, c)
		@widget, @index, @title, @type, @complete = w, i, ti, ty, c
	end
  
end
