# coding: utf-8

class EditionContact < Prawn::Document
	
	def initialize chemin
	
		super(	:page_layout => :portrait,
						:right_margin => 15,
						:page_size => 'A4'
				 )
		
		font_size 10
		contacts = Contact.includes(:coordonnees).order(:societe, :nom)
		
		preparation contacts
	
		rendu chemin
	end
	
	def preparation contacts
			
		en_tete
		corps contacts
		pieds
		
	end
	
	def en_tete	

		text "<b>Liste des contacts</b>", :size => 30, :inline_format => true, :align => :center
		move_down 20
		
	end
	
	def corps contacts
		
		column_box([bounds.left, cursor], :columns => 2, :width => bounds.width, :height => bounds.height-80) do
			res_array = []
			lettre = nil
			contacts.each do |contact|
				if lettre != contact.societe.to_s[0]
					lettre = contact.societe.to_s[0]
					move_down 5
					rectangle [bounds.left_side-40,cursor+3], bounds.width, 15
					fill_color "dddddd"
					stroke_color "000000"
					fill_and_stroke()
					fill_color "000000"
					text "<b>#{lettre}</b>", :align => :center, :inline_format => true
					move_down 5
				end
					
				text "<b>#{contact.societe.to_s.upcase}</b>", :inline_format => true
				text contact.nom.to_s, :indent_paragraphs => 15
				coordonnees = Coordonnee.includes(:typecoordonnee).where(:contact_id => contact.id).order(:typecoordonnee_id)
				coordonnees.each do |c|
					text "#{c.typecoordonnee.designation}: #{c.texte}", :indent_paragraphs => 15
				end
			end
		end
	end
	
	def pieds
		number_pages "<page>/<total>", {:start_count_at => 1, :at => [bounds.right - 100, 0], :align => :right, :size => 8}
		
		go_to_page(1)
		width = (bounds.width/2)-10
		line [width,0], [width,bounds.height-50]
		stroke
		
		if page_count>1
			(2..page_count).to_a.each do |i|
				go_to_page(i)
				line [width,0], [width,bounds.height]
				stroke
			end
		end
		
	end
	
	def rendu fichier

		render_file fichier
	
	end
	
end
