# coding: utf-8

class ListeContact_box < Gtk::VBox

	attr_reader :view, :search_gencode, :add_new, :nombre
	
	def initialize window, research=false
	
		super(false, 3)
		
		set_border_width 10
		
		@window = window
		@research = research
		 
		agencement
	
	end
	
	def agencement
	
		vbox = Gtk::VBox.new false, 3
		@add_new = Gtk::Button.new
		hboxajout = Gtk::HBox.new false, 2
		hboxajout.add Gtk::Image.new( "./resources/add.png" )
		hboxajout.add Gtk::Label.new "Nouveau contact"
		@add_new.add hboxajout
		
		@imprimer = Gtk::Button.new
		hbox_imprimer = Gtk::HBox.new false, 2
		hbox_imprimer.add Gtk::Image.new( "./resources/print.png" )
		hbox_imprimer.add Gtk::Label.new "Imprimer la liste"
		@imprimer.add hbox_imprimer
		
		hbox_buttons = Gtk::HBox.new false, 2
		hbox_buttons.add @imprimer
		hbox_buttons.add @add_new
		
		align_button = Gtk::Alignment.new 1, 0, 0, 0
		align_button.add hbox_buttons
		frame = Gtk::Frame.new "Liste des contacts"
		@compteur = Gtk::Label.new
		
		hbox = Gtk::HBox.new false, 3
		@search = Gtk::Entry.new
		@search.primary_icon_stock = Gtk::Stock::FIND
		@search.secondary_icon_stock = Gtk::Stock::CLEAR
		
		# Agencement
		hbox.add @search
		hbox.add align_button
		
		vbox.pack_start hbox, false, false, 3 unless @research
		vbox.pack_start tableau_contacts, true, true, 3 
		vbox.pack_start @compteur, false, false, 3 

		frame.add vbox
		
		@add_new.signal_connect( "clicked" ) {
			@window.contact.refresh
			@window.affiche @window.contact
		}
		
		@imprimer.signal_connect( "clicked" ) { imprimer }
		
		@search.signal_connect('activate') {
			refresh
		}
		
		add frame
	
	end
	
	def tableau_contacts
	
		# list_store 					           (id      soc     nom     tel pro tel_perso portable mail)
		@list_store = Gtk::TreeStore.new(Integer, String, String, String, String, String, String, String)
		@view = Gtk::TreeView.new(@list_store)
		@view.signal_connect ("row-activated") { |view, path, column|
			if !@research then
				@window.contact.refresh @view.model.get_value @view.selection.selected, 0
				@window.affiche @window.contact
			end
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		#renderer_left.background = "white"
		renderer_left.xalign = 0
		renderer_left.yalign = 0
		
		col = Gtk::TreeViewColumn.new("id", renderer_left, :text => 0)
		col.sort_column_id = 0
		col.resizable = true
		#@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Société/Nom", renderer_left, :text => 1)
		col.sort_column_id = 1
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Nom secondaire", renderer_left, :text => 2)
		col.sort_column_id = 2
		col.resizable = true
		@view.append_column(col) unless @research
		
		col = Gtk::TreeViewColumn.new("Tél. Bureau", renderer_left, :text => 3)
		col.sort_column_id = 3
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Tél. Domicile", renderer_left, :text => 4)
		col.sort_column_id = 4
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Tél. Portable", renderer_left, :text => 5)
		col.sort_column_id = 5
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("E-Mail Pro.", renderer_left, :text => 6)
		col.sort_column_id = 6
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("E-Mail Perso.", renderer_left, :text => 7)
		col.sort_column_id = 7
		col.resizable = true
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
  	scroll.add @view
  	
  	scroll
	
	end
	
	def refresh
		
		term = @search.text unless @search.text.empty?
		contacts = Contact.order(:societe, :nom)
		contacts = contacts.where("societe ILIKE ? OR nom ILIKE ?", "%#{term}%", "%#{term}%") if term
		
		@nombre = contacts.count
		
		@compteur.text = "Nombre d'éléments dans la liste : #{contacts.count}"
		
		if contacts then
			if contacts.count.eql?(1) then
				@window.contact.refresh contacts.first.id
				@window.affiche @window.contact
				@window.contact.focus
			else		
				remplir contacts
			end
		end	
	
	end
	
	def remplir res

		@list_store.clear
		# list_store (id      soc     nom     tel pro tel_perso portable mail)
		res.each { |h|			
			iter = @list_store.append nil
			iter[0] = h.id
			iter[1] = h.societe
			iter[2] = h.nom
			coordonnees = Coordonnee.where(:contact_id => h.id)
			coordonnees.each { |c|
				iter[3] = c.texte if c.typecoordonnee_id.eql?(3)  # Tél bureau
				iter[4] = c.texte if c.typecoordonnee_id.eql?(4)  # Tél domicile
				iter[5] = c.texte if c.typecoordonnee_id.eql?(5)  # Tél portable
				iter[6] = c.texte if c.typecoordonnee_id.eql?(2)  # Mail pro
				iter[7] = c.texte if c.typecoordonnee_id.eql?(1)  # Mail perso
			}
		}
			
	end
	
	def remplir_filtre
		@filtre.model.clear
		
	end
	
	def changement_filtre
		#refresh search=nil, tiers=nil, @filtre.active if @filtre.active>-1
	end
	
	def focus
		@search.grab_focus
	end
	
	def imprimer
		
		chemin = "#{@window.config_db.chemin_temp}/contact.pdf"
		EditionContact.new chemin
		if RUBY_PLATFORM.include?("linux") then 
			system "xdg-open", chemin
		elsif RUBY_PLATFORM.include?("mingw") then 
			system "start", chemin
		else
			# TODO
		end
	end
	
end

